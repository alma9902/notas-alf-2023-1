\documentclass[12 pt]{article}

%format 
\usepackage[margin = 2.5 cm]{geometry}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}

%images
\usepackage{graphicx}
\graphicspath{{./images/}}
\usepackage{float}

%font 
\usepackage{lmodern}

%mathequations 
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{mathrsfs}

% Referencias con nombres automaticos
\usepackage{cleveref}

% Definiciones de nuevos ambientes
\newtheorem{teorema}{Teorema}[section]
\newtheorem{lema}[teorema]{Lema}
\newtheorem{proposicion}[teorema]{Proposici\'on}
\newtheorem{corolario}[teorema]{Corolario}

\theoremstyle{definition}
\newtheorem{definicion}[teorema]{Definici\'on}

% Nombres para cleveref
\crefname{teorema}{el Teorema}{los Teoremas}
\crefname{lema}{el Lema}{los Lemas}
\crefname{proposicion}{la Proposici\'on}{las Proposiciones}
\crefname{corolario}{el Corolario}{los Corolarios}
\crefname{algorithm}{el Algoritmo}{los Algoritmos}
\crefname{section}{la Secci\'on}{las Secciones}
\crefname{figure}{la Figura}{las Figuras}


% Delimitadores para terminar las demostraciones
\newcommand{\blackqed}{\hfill$\blacksquare$}
\newcommand{\whiteqed}{\hfill$\square$}
\newcounter{proofcount}

% Redefinicion para que las demostraciones terminen con cuadrito negro
\renewenvironment{proof}[1][\proofname.]{\par
\ifnum \theproofcount>0 \pushQED{\whiteqed} \else \pushQED{\blackqed} \fi%
\refstepcounter{proofcount}
%
\normalfont %\topsep6\p@\@plus6\p@\relax
\trivlist
\item[\hskip\labelsep
\itshape
\textbf{\textit{#1}}]\ignorespaces
}{%
\addtocounter{proofcount}{-1}
\popQED\endtrivlist
}

% Macros
% Abreviatura para fuentes true type
\newcommand{\ttt}[1]{%
\texttt{#1}%
}

\newcommand{\indice}[1]{%
\textbf{#1}\index{#1}%
}


%header 
\title{
    Notas de clase 
}
\author{
    Autómatas y Lenguajes Formales, Facultad de Ciencias, UNAM
}
\date{
    25 de agosto del 2022
}

%color for possible tables 
\usepackage[table]{xcolor}
\usepackage{multirow}

\begin{document}
    \maketitle

    \section{Lenguajes}
			A lo largo de nuestra vida hemos usado el lenguaje como medio de
			comunicación con los demás. Sabemos que hay algunos lenguajes naturales
			que son familiares para nosotros, tales como el español, inglés y francés
			o lenguajes de programación como Java y Python entre otros. En este curso
			usaremos la palabra \texttt{lenguaje} de manera más general, tomando
			cualquier conjunto de cadenas sobre un alfabeto de símbolos. Decimos que
			un \texttt{alfabeto} es un conjunto no vacío de símbolos. Por convención,
			un alfabeto es denotado por $\Sigma$. Algunos alfabetos comunes son: 

			\begin{enumerate}
				\item $\Sigma = \{ 0,1\}$, el alfabeto binario
				\item $\Sigma = \{ a,\dots, z\}$, el conjunto de todas las letras 
				minúsculas. 
				\item El conjunto de todos los caracteres ASCII, o el conjunto de todos 
				los caracteres de ASCII imprimibles 
			\end{enumerate}

		Una cadena es una sucesión finita de símbolos tomados de algún alfabeto
		$\Sigma$. Por ejemplo, la sucesión de caracteres $01010111$ es una cadena
		sobre el alfabeto binario. Decimos que una cadena es \texttt{vacía} si no
		tiene algún caracter. La cadena vacía es denotada por $\varepsilon$ y no
		importa cuál sea el alfabeto, $\varepsilon$ es una cadena sobre $\Sigma$.
		Por definición la longitud de la cadena vacía es igual a cero. En algunas
		ocasiones, es útil clasificar a las cadenas de acuerdo a su longitud. Por
		ejemplo, la cadena $011011$ tiene longitud $6$. Si $x$ es una cadena, la
		notación estándar para referirnos a su longitud es $|x|$. 

		Si $\Sigma$ es un alfabeto, podemos expresar el conjunto de todas las
		cadenas de determinada longitud sobre $\Sigma$ usando notación exponencial.
		Decimos que el conjunto de cadenas de longitud $k$ es $\Sigma^k$ y cada uno
		de los símbolos están en $\Sigma$. Como existe una única cadena de longitud
		cero, entonces sin importar cuál alfabeto sea, $\Sigma^0 = \{
		\varepsilon\}$. Con esto último en mente, si tomamos de nuevo el alfabeto
		binario $\Sigma = \{0,1 \}$, $\Sigma^0 = \{\varepsilon \}$, $\Sigma^1 = \{
		0,1\}$, $\Sigma^2 = \{ 00,01,10,11\}$ y $\Sigma^3 = \{ 000, 001, 010, 011,
		100, 101, 110, 111\}$. Notemos que puede existir una ligera confusión entre
		$\Sigma$ y $\Sigma^1$, la diferencia está en que $\Sigma$ es un alfabeto,
		por lo que sus elementos son símbolos mientras que en $\Sigma^1$, los
		elementos son cadenas de longitud $1$. No vamos a usar notación distinta
		para los dos conjuntos debido a que siempre será claro (de acuerdo al
		contexto) si nos estamos refiriendo a alfabetos o a conjunto de cadenas. 

		El conjunto de todas las cadenas sobre un alfabeto $\Sigma$ es denotado por
		$\Sigma^*$, así, 
		
		\[\{ 0,1\}^* = \{ \varepsilon, 0,1, 00,01,10,11,0000, \dots \}\]

		 También podemos ver a $\Sigma^*$ como 
		\[\Sigma^* = \Sigma^0 \cup \Sigma^1 \cup \Sigma^2 \dots\]     

		Notemos que hemos listado las cadenas en $\{ 0,1\}^*$ en \texttt{orden
		canónico}, esto es, las cadenas cortas preceden a las de mayor longitud y
		las cadenas de la misma longitud aparecen en orden alfabético. El orden
		canónico es distinto del \texttt{orden lexicográfico} u orden estrictamente
		alfabético ya que en el segundo, $00$ precede a la cadena $1$. La diferencia
		radica en que el orden canónico puede ser descrito haciendo una sola lista
		de cadenas que incluye cada elemento de $\Sigma^*$ exactamente una vez. Si
		quisiéramos describir un algoritmo que hiciera algo con cada cadena en
		$\{0,1 \}^*$, tiene más sentido decir que vamos a considerar todas las
		cadenas en orden canónico y para cada cadena vamos a realizar alguna acción.
		Si por otra parte, consideramos el orden lexicográfico, comenzaría por
		considerar a las cadenas $\varepsilon, 0, 00, 000, 0000, \dots$ y podría
		nunca considerar a la cadena $1$. 
		
		La operación básica en cadenas es la \texttt{concatenación}. Si $x$ y $y$
		son dos cadenas sobre un alfabeto, la concatenación de $x$ y $y$ es escrita
		$xy$ y consiste en los símbolos de $x$ seguidos de los de $y$. Si $x = ab$ y
		$y = bab$, por ejemplo, entonces $xy = abbab$ y $yx = babab$. Cuando
		concatenamos la cadena vacía $\varepsilon$ con cualquier otra cadena, el
		resultado es la otra cadena (para toda cadena $x$, $x \varepsilon =
		\varepsilon x = x$); y para cada $x$, si una de las fórmulas $xy = x$ o $yx
		= x$ es verdadera para alguna cadena $y$, entonces $y = \varepsilon$. En
		general, para dos cadenas $x$ y $y$, $|xy| = |x| + |y|$. 
		
		La \texttt{concatenación} es una operación asociativa, esto es, $(xy)z =
		x(yz)$ para cualesquiera cadenas $x,y$ y $z$. Esto nos permite escribir
		$xyz$ sin especificar cómo son agrupados los factores. 


		Si $s$ es una cadena y está compuesta de tres cadenas $t,u$ y $v$, i.e, $s =
		tuv$, entonces $t$ es un \texttt{prefijo} de $s$, $v$ es un \texttt{sufijo}
		de $s$ y $u$ es una subcadena de $s$. Como $t$ y $u$ pueden ser cadenas
		vacías, los prefijos y sufijos son casos especiales de subcadenas. La cadena
		$\varepsilon$ es prefijo de todas las cadenas, un sufijo de todas las
		cadenas y una subcadena de cada cadena y además, cada cada es prefijo,
		sufijo y subcadena de su misma cadena. 

		Un \texttt{lenguaje} es un subconjunto de cadenas tomadas de $\Sigma^*$,
		i.e, si $\Sigma$ es un alfabeto y $L \subset \Sigma^*$, entonces $L$ es un
		lenguaje sobre $\Sigma$. Observemos que un lenguaje sobre $\Sigma$ no
		necesita incluir cadenas con todos los símbolos del alfabeto. A continuación
		están algunos lenguajes sobre $\{ a,b\}$: 
		
		\begin{enumerate}
			\item $\{\varepsilon, a, aab \}$
			\item El lenguaje de las cadenas palíndromas sobre $\{ a,b\}$
			\item $\{ x \in \{a,b \}^* \colon |x| \ge 2 \}$
			\item EL lenguaje vacío
		\end{enumerate}

		La cadena vacía siempre es un elemento de $\Sigma^*$ pero otros languajes
		sobre $\Sigma$ pueden o no contenerla, de los ejemplos enlistados, sólo el
		primero y el segundo contienen a $\varepsilon$. 

		Como los lenguajes son conjuntos, una manera de formar nuevos lenguajes a
		partir de existentes es usando operaciones de conjuntos. Si $L_1$ y $L_2$
		son lenguajes sobre algún alfabeto $\Sigma$, $L_1 \cup L_2$, $L_1 \cap L_2$
		y $L_1 - L_2$ son también lenguajes sobre $\Sigma$. También podemos usar la
		concatenación de cadenas para construir un nuevo lenguaje. Si $L_1$ y $L_2$
		son lenguajes sobre $\Sigma$, la concatenación de $L_1$ y $L_2$ es el
		lenguaje 
		\[L_1 L_2 = \{ xy \colon x \in L_1 \text{ y } y \in L_2\}\]

		Por ejemplo, la concatenación de los lenguajes $L_1 = \{ a,aa\}$ y $L_2 = \{
		\varepsilon, b, ab\}$ es $L_1L_2  = \{a,ab,aab, aa, aaab \}$. 

		Nuevamente, podemos adoptar la notación exponencial pero ahora para la
		concatenación de $k$ copias de un sólo símbolo $a$, de una cadena $x$ o de
		un sólo lenguaje $L$. Si $k > 0$, entonces $a^k = aa \dots a$, donde hay $k$
		ocurrencias de $a$ y de manera análoga ocurre para $x^k$ y $L^k$. La
		notación exponencial es útil porque podemos tener que $a^i a^j = a ^{i +
		j}$, $x^ix^j = x ^{i +j}$ y $L^i L^j = L ^{i+j}$. En caso de que $i = 0$,
		las primeras dos fórmula requieren que definamos $a^0$ y $x^0$ igual a la
		cadena vacía $\varepsilon$, para la última fórmula definimos $L ^0 = \{
		\varepsilon \}$. 

		Para un lenguaje $L$ sobre un alfabeto $\Sigma$, el lenguaje de todas las
		cadenas que se pueden obtener a partir de la concatenación de cero o más
		cadenas en $L$ es denotado por $L ^*$ Esta operación en un lenguaje $L$ es
		conocida como la estrella de Kleene. La notación $L^*$ es consistente con la
		de $\Sigma^*$, en la cual podemos describir el conjunto de cadenas que
		obtenemos al concatenar cero o más cadenas de longitud $1$ sobre $\Sigma$.
		$L^*$ puede ser definido por la fórmula 

		\[L ^* = \bigcup \{ L ^k \colon k \in \mathcal{N}\}\]

		Cuando describimos lenguajes usando fórmulas que usan operaciones como la
		unión, concatenación y la estrella de Kleene, usaremos reglas de jerarquía
		similares a las reglas algebraicas a las que estamos acostumbrados. Por
		ejemplo, la fórmula $L_1 \cup L_2 L_3^* = L_1 \cup (L_2(L_3 ^*))$; de las
		tres operaciones, la que tiene mayor jerarquía o precedencia es la estrella
		de Kleene, luego es la concatenación y la menor es la unión. Así, las
		expresiones $(L_1 \cup L_2)L_3^*$, $L_1 \cup (L_2L_3^*)$ y $(L_1\cup
		L_2L_3)^*$ son diferentes lenguajes. 
		

	\section{Definiciones recursivas}

	Como sabemos, la recursión es una técnica que a veces es útil cuando
	escribimos programas. En esta sección vamos a considerarla como herramienta
	para definir conjuntos. Una definición recursiva de un conjunto comienza con
	un \texttt{caso base} que especifica uno o más elementos en el conjunto. La
	parte \texttt{recursiva} de la definición incluye una o más operaciones que
	pueden ser aplicadas a los elementos que ya sabemos que están en el conjunto
	para producir u obtener nuevos elementos. 

	Como una manera de definir un conjunto tiene diversas ventajes porque permite
	definiciones concisas, permite una manera natural de definir funciones en el
	conjunto así como también una manera natural de demostrar que alguna condición
	o propiedad es satisfacida para cada elemento del conjunto. 

	Un ejemplo clásico de una definición recursiva es la definición del conjunto
	$\mathcal{N}$ de los números naturales. Asumimos que $0$ es un número natura y
	que tenemos una operación sucesor, en la que, para cada número natural $n$,
	nos da otro número que es el sucesor de $n$ y puede ser escrito como $n+1$. La
	definición es la siguiente: 

	\begin{enumerate}
		\item $0 \in \mathcal{N}$
		\item Para cada $n \in \mathcal{N}$, $n+1 \in \mathcal{N}$
		\item Cada elemento de $\mathcal{N}$ puede obtenerse con la regla $1$ o $2$
	\end{enumerate}

	Para obtener algún elemento de $\mathcal{N}$, usamos la regla $1$ una vez y
	luego la regla $2$ un número finito de veces (cero o más). Por ejemplo, para
	tener el número natural $8$, usamos la regla $1$ para tener el cero, luego con
	la segunda regla obtenemos $n = 1$ (porque es el sucesor de cero), después
	nuevamente aplicamos la regla dos para obtener $n = 2$ y así sucesivamente
	aplicamos la regla dos hasta que $n = 7$. 

	Podemos resumir las primeras dos reglas diciendo que $\mathcal{N}$ contiene al
	número $0$ y que es cerrado bajo la operación sucesor. La tercera regla en la
	definición es para dejar claro que el conjunto que estamos definiendo es el
	conjunto que contiene \textit{solamente} los números obtenidos a partir de
	aplicar la regla uno una vez y la regla dos un número finito de veces. En
	otras palabras, $\mathcal{N}$ es el conjunto más pequeño de números que
	contiene al cero y es cerrado bajo la operación sucesor. 

	En los ejemplos vistos en clase y en estas notas, omitiremos la regla que
	corresponde a la tercera en este ejemplo, pero cuando definamos un conjunto de
	manera recursiva, asumiremos que una regla como esta aplica aunque no sea
	puesta explícitamente. 

	Así como una función recursiva en algún programa que va a ser ejecutado tiene
	una cláusula de escape para evitar llamarse a sí mismo por siempre, una
	definición recursiva, como la del ejemplo anterior, debe contar con al menos
	un  \texttt{caso base} que nos indica un elemnto en el conjunto. La regla
	recursiva ($n+1 \in \mathcal{N}$ en el ejemplo) trabaja junto con el caso base
	para obtener todos los elementos que faltan en el conjunto. 

	\subsection{Algunos ejemplos}
	
	\begin{itemize}
		\item \textbf{Definición recursiva de $\{a,b\}^*$}. 
		
		Nuestra definición recursiva de $\mathcal{N}$ comenzó con el número cero, y
		la regla recursiva nos permitió tomar algún número arbitrario $n$ para
		obtener un número natural que es mayor y cuya diferencia es uno. La
		definición recursiva para $\{a,b \}^*$ es análoga, empezamos con la cadena
		de longitud cero y decimos cómo tomar una cadena arbitraria $x$ para obtener
		cadenas de longitud $|x|+1$. Las reglas son: 

			\begin{enumerate}
				\item $\varepsilon \in \{a,b \}^*$
				\item Para cada $x \in \{a,b \}^*$, las cadenas $xa$ y $xb$ también
				pertenecen a $\{ a,b\}^*$
			\end{enumerate}
		
		\item  \textbf{Definición del lenguaje} $AnBn = \{ a^nb^n \colon n \in \mathcal{N} \}$. 
		
		Notemos que la cadena más corta en $AnBn$ es la cadena vacía, y si tenemos
		un elemento $a^ib^i$ de longitud $2i$, la manera de tener un elemento de
		longitud $2i +2 $ es añadiendo una $a$ al inicio y una $b$ al final, por lo
		que la definición recursiva de $AnBn$ es: 
			
			\begin{enumerate}
				\item $\varepsilon \in AnBn$
				\item Para cada $x \in AnBn$, $axb \in AnBn$
			\end{enumerate}
		
			\item \textbf{Definición de las expresiones algebraicas.}
			
			Las expresiones algebraicas más simples usan los operadores $+$ y $*$ con
			números y/o variables permitidos como operandos. La definición recursiva
			es: 

			\begin{enumerate}
				\item Cualquier número o variable $x$ es una expresión aritmética
				\item Si $x$ y $y$ son expresiones aritméticas, también lo son $x+y$, $x
				* y$ y $(x)$
			\end{enumerate}
			
			\item \textbf{Lenguaje de los paréntesis balanceados} Diremos que el
			lenguaje de los paréntesis balanceados es \textit{Bal}. Todas las cadenas
			que pertenecen a \textit{Bal} tienen la misma cantidad de paréntesis que
			abren y que cierran. Las reglas que lo generan son: 

			\begin{enumerate}
				\item $\varepsilon \in \text{Bal}$
				\item Para cada $x$ y $y$ en $\text{Bal}$, $xy \in \text{Bal}$ y $(x)
				\in \text{Bal}$
			\end{enumerate}
	\end{itemize}

	\section{Inducción estructural}

	\begin{proposicion}
		Todas las cadenas de las expresiones algebraicas definidas en la sección
		anterior tienen mismo número de paréntesis izquierdos y derechos. 
		\label{prop:mismo-num-parentesis-expr-algebraicas}
	\end{proposicion}
	\begin{proof}
		Procederemos por inducción estructural. Sea $s$ una cadena que pertenece al
		lenguaje de las expresiones algebraicas. Si $s$ es definada por el caso base
		(regla $1$), entonces $s$ es un número o una variable y no tiene paréntesis
		por lo que la proposición se cumple. Por hipótesis de inducción, suponemos
		que existen dos cadenas arbitrarias $x$ y $y$ en las expresiones aritméticas
		que tienen el mismo número de paréntesis izquierdos y derechos. Diremos que
		el número de paréntesis (izquierdos o derechos) que tiene la cadena $x$ es
		$n$ mientras el número de ocurrencias en $y$ es $m$. Sea $z$ una cadena
		formada a partir de $x$ y $y$. Tenemos tres posibles casos. Si $ z = x + y$,
		entonces $z$ tiene $n + m$ paréntesis izquierdos y $n+m$ paréntesis derechos
		por lo que se sigue cumpliendo la proposición. Luego, si $z = x * y$, es
		análogo al caso anterior puesto que el número de paréntesis izquierdos es $n
		+m$ al igual que el número de paréntesis derechos por hipótesis inductiva.
		Finalmente, si $z = (x)$, entonces hay $n+1$ paréntesis izquierdos, $n$
		paréntesis corresponden a $x$ y uno es añadido explícitamente. De la misma
		forma ocurre con el número de paréntesis derechos, así que $z$ tiene el
		mismo número de paréntesis izquierdos y derechos. 
	\end{proof}

	Ahora queremos demostrar que una cadena $x$ pertenece al lenguaje \ttt{Bal}
	(definido en la sección anterior) si y solo si la propiedad $B(x)$ que está en
	\cref{prop:parentesis-balanceados} es verdadera. Para la primera parte,
	tenemos que mostrar que toda cadena $x$ que está en el lenguaje \ttt{Bal}
	cumple la condición $B(x)$. Para esto vamos a usar inducción estructural
	porque tenemos la definición recursiva para \ttt{Bal}. Para la segunda parte
	de la demostración no podemos usar la misma técnica porque estamos tratando de
	probar que doa cadena de paréntesis, no sólo todas las cadenas en \ttt{Bal},
	satisfacen alguna propiedad. 

	\begin{proposicion}
		$B(x) \colon x$ contiene la misma cantidad de paréntesis izquierdos y
		derechos, además no hay prefijos de $x$ que contengan más paréntesis
		derechos que izquierdos. 
		\label{prop:parentesis-balanceados}
	\end{proposicion}
	\begin{proof}
		Primero veamos que toda cadena que pertenece a \ttt{Bal} cumple con la
		condición de \cref{prop:parentesis-balanceados}. Notemos que se cumple para
		la cadena vacía $\varepsilon$. Como $\varepsilon$ no tiene algún paréntesis,
		entonces $B(\varepsilon)$ es verdadera. Sean dos cadenas $x$ y $y$ que
		pertenecen al lenguaje \ttt{Bal} tal que $B(x)$ y $B(y)$ son ciertas. Sea
		$z$ una cadena formada por $x$ y $y$, sabemos que existen únicamente dos
		maneras de obtener a $<$, $X = xy$ o bien $z = (x)$. Si $z = xy$, entonces
		como $x$ y $y$ tienen mismo número de paréntesis izquierdos y derechos,
		también lo tiene $z$. Si $z'$ es un prefijo de $xy$, entonces $z'$ es un
		prefijo de $x$ o $z' = xw$ es algún prefijo $w$ de $y$. En el primer caso,
		como se cumple por hipótesis inductiva $B(x)$, implica que $z'$ no puede
		tener más paréntesis derechos que izquierdos. En el segundo caso, como $x$
		tiene número igual de paréntesis izquierdos que derechos, $w$ no puede tener
		más derechos que izquierdos por lo que $z' = xw$ tampoco puede tener más
		derechos que izquierdos. Por otra parte, si la cadena formada $z = (x)$,
		entonces es claro que el número de paréntesis izquierdos y derechos es igual
		porque se agrega uno al inicio y al final de la cadena $x$. Por esa misma
		razón, si $z'$ es un prefijo de $(x)$, puede ser que $z' = (x)$ o que $z' =
		wx)$ donde $w$ es un prefijo de $x$, como $B(x)$ es cierta, entonces en
		ningún caso, $z'$ contiene más paréntesis derechos que izquierdos.  
	\end{proof}
	\begin{proposicion}
		\label{prop:lenguaje-AnBn}
	\end{proposicion}
	\begin{proposicion}
		\label{prop:reverse-function}
	\end{proposicion}
\end{document}